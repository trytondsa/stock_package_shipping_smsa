# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import carrier
from . import stock

__all__ = ['register']


def register():
    Pool.register(
        stock.Package,
        stock.ShipmentOut,
        stock.ShipmentInReturn,
        carrier.Carrier,
        carrier.CredentialSMSA,
        module='stock_package_shipping_smsa', type_='model')
    Pool.register(
        stock.CreateShipping,
        stock.CreateShippingSMSA,
        module='stock_package_shipping_smsa', type_='wizard')
    Pool.register(
        module='stock_package_shipping_smsa', type_='report')
