# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import (
    MatchMixin, ModelSQL, ModelView, fields, sequence_ordered)
from trytond.pool import PoolMeta
from trytond.pyson import Eval

class CredentialSMSA(sequence_ordered(), ModelSQL, ModelView, MatchMixin):
    'SMSA Credential'
    __name__= 'carrier.credential.smsa'

    company = fields.Many2One('company.company', 'Company')
    username = fields.Char('Username', required=True)
    email = fields.Char('Email', required=True)
    api_key =  fields.Char('API Key', required=True)
    server = fields.Selection([
            ('testing', 'Testing'),
            ('production', 'Production'),
            ], 'Server')

    @classmethod
    def default_server(cls):
        return 'testing'
    
class Carrier(metaclass=PoolMeta):
    __name__ = 'carrier'

    smsa_service_type= fields.Selection([
        (None, ''),
        ('DD' , 'SMSA Priority Documents (SPOD)'),
        ('DP' , 'SMSA Priority Parcels (SPOP)'),
        ('SFOP' , 'SMSA First Priority Parcels (SFOP)'),
        ('SFS' , 'SMSA Freight Service Loose (SFS)'),
        ('SFSP' , 'SMSA Freight Service Palletized (SFSP)'),
        ('SL' , 'SMSA International Documents (SIDX) SMSA Envelop'),
        ('SI' , 'SMSA International NON Documents (SIND)'),
        ('SP' , 'SMSA International Documents (SIDX) SMSA PAK'),
        ('S10' , 'SMSA International 10KG Box'),
        ('S25' , 'SMSA International 25KG Box'),
        ('SFOD' , 'SMSA First Priority Documents (SFOD)'),
        ('SIES' , 'SMSA International Economy (SIES)'),
        ('PR' , 'Promo Service'),
        ('SIPBR' , 'New SMSA International Parcel by Road'),
        ('DE' , 'SMSA Domestic Express Services Egypt'),
        ('SIDG' , 'SMSA International Dangerous Goods (SIDG)'),
        ('DGFP' , 'DG Domestic - Freight Pallets'),
        ('SDDG' , 'SMSA DG Domestic Parcel'),
        ('DGFL' , 'DG Domestic - Freight Loose'),
    ], 'Service Type', sort= False, translate=False,
    states={
        'required': Eval('shipping_service') == 'smsa',
        'invisible': Eval('shipping_service') != 'smsa',
    })

    @classmethod 
    def __setup__(cls):
        super(Carrier, cls).__setup__()
        cls.shipping_service.selection.append(('smsa', "SMSA Express"))
    
    @classmethod
    def view_attributes(cls):
        return super(Carrier, cls).view_attributes() + [
            ("/form/separator[@id='smsa']", 'states', {
                    'invisible': Eval('shipping_service') != 'smsa',
                    }),
            ]
    
    @property
    def shipping_label_mimetype(self):
        mimetype = super().shipping_label_mimetype
        if self.shipping_service == 'smsa':
            mimetype = 'application/pdf'
        return mimetype