*************
Configuration
*************

The *Stock Package Shipping SMSA Module* uses values from the settings in the
``[stock_package_shipping_ups]`` section of Trytond configuration file


``requests_timeout``
====================

The ``requests_timeout`` defines the time in seconds to wait for the SMSA APIs
answer before failing.

The default value is: ``300``