##################################
Stock Package Shipping SMSA Module
##################################

The *Stock Package Shipping SMSA Module* allows to generate `SMSA Express
<https://www.smsaexpress.com/>`_ labels per package using `their APIs <https://documenter.getpostman.com/view/3109892/2s93JoymdR>`.

.. toctree::
   :maxdepth: 2

   usage
   configuration
   design
   releases
