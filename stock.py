import base64
import re
import ssl
import urllib.parse
import json
from itertools import zip_longest
from decimal import Decimal

import requests

from trytond.config import config
from trytond.i18n import gettext
from trytond.model import fields
from trytond.model.exceptions import AccessError

from trytond.modules.stock_package_shipping.exceptions import (
    PackingValidationError)
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.wizard import StateAction, StateTransition, Wizard

from .exceptions import SMSAError

SERVER_URLS = {
    'testing': 'https://b2bapis-sandbox.azurewebsites.net/api/Shipments',
    'production': 'https://b2b.smsaexpress.com/api/Shipments',
    }
TRACKING_URL = 'https://www.smsaexpress.com/trackingdetails'
TIMEOUT = config.getfloat(
    'stock_package_shipping_smsa', 'requests_timeout', default=300)

class Package(metaclass=PoolMeta):
    __name__ = 'stock.package'

    def get_shipping_tracking_url(self, name):
        pool = Pool()
        ShipmentOut = pool.get('stock.shipment.out')
        ShipmentInReturn = pool.get('stock.shipment.in.return')
        url = super().get_shipping_tracking_url(name)
        if (self.shipping_reference
                and self.shipment
                and self.shipment.id >= 0
                and self.shipment.carrier
                and self.shipment.carrier.shipping_service == 'smsa'):
            party = address = None
            if isinstance(self.shipment, ShipmentOut):
                party = self.shipment.customer
                address = self.shipment.delivery_address
            elif isinstance(self.shipment, ShipmentInReturn):
                party = self.shipment.customer
                address = self.shipment.delivery_address
            #TODO add language 
            parts = urllib.parse.urlsplit(TRACKING_URL)
            query = urllib.parse.parse_qsl(parts.query)
            query.append(('tracknumbers[0]', self.shipping_reference))
            parts = list(parts)
            parts[3] =  urllib.parse.urlencode(query)
            url = urllib.parse.urlunsplit(parts)
        return url
    

class ShippingSMSAMixin:
    __slots__ = ()

    def validate_packing_smsa(self, usage= None):
        warehouse = self.shipping_warehouse
        if not warehouse.address:
            raise PackingValidationError(
                gettext('stock_package_shipping_smsa'
                        '.msg_warehouse_address_required',
                        shipment=self.rec_name,
                        warehouse=warehouse.rec_name))
        for address in [self.shipping_to_address, warehouse.address]:
            if not address.contact_mechanism_get(
                {'phone', 'mobile'}, usage=usage):
                raise PackingValidationError(
                    gettext('stock_package_shipping_smsa'
                            '.msg_phone_required',
                            shipment=self.rec_name,
                            address=address.rec_name))
            #TODO needs refactoring
            if not address.street:
                raise PackingValidationError(
                    gettext('stock_package_shipping_smsa'
                            '.msg_street_required',
                            shipment=self.rec_name,
                            address=address.rec_name))
            if not address.city:
                 raise PackingValidationError(
                    gettext('stock_package_shipping_smsa'
                            '.msg_city_required',
                            shipment=self.rec_name,
                            address=address.rec_name)) 
            if not address.country:
                 raise PackingValidationError(
                    gettext('stock_package_shipping_smsa'
                            '.msg_country_required',
                            shipment=self.rec_name,
                            address=address.rec_name))



class ShipmentOut(ShippingSMSAMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out' 


class ShipmentInReturn(ShippingSMSAMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.in.return'


class CreateShipping(metaclass=PoolMeta):
    __name__ = 'stock.shipment.create_shipping'

    smsa = StateAction(
        'stock_package_shipping_smsa.act_create_shipping_smsa_wizard'
    )

    def transition_start(self):
        next_state = super(CreateShipping, self).transition_start()
        if self.record.carrier.shipping_service == 'smsa':
            next_state = 'smsa'
        return next_state
    
    def do_smsa(self, action):
        ctx= Transaction().context
        return action, {
            'model': ctx['active_model'],
            'id': ctx['active_id'],
            'ids': [ctx['active_id']]
        }


class CreateShippingSMSA(Wizard):
    'Create SMSA Express Shipping'
    __name__= 'stock.shipment.create_shipping.smsa'

    start = StateTransition()

    def transition_start(self):
        pool = Pool()
        Package = pool.get('stock.package')

        shipment = self.record
        if shipment.reference:
            raise AccessError(
                gettext('stock_package_shipping_smsa'
                        '.msg_shipment_has_reference_number',
                        shipment=shipment.rec_name))

        #TODO needs to validate                  
        # if not shipment.shipping_description:
        #     raise PackingValidationError(
        #         gettext('stock_package_shipping_smsa'
        #                 '.msg_shipping_description_required',
        #                 shipment=shipment.rec_name))

        credential = self.get_credential(shipment)
        carrier = shipment.carrier
        packages = shipment.root_packages
        shipment_request = self.get_request(shipment, packages, credential)
        api_url= config.get('stock_package_shipping_smsa', credential.server,
                            default= SERVER_URLS[credential.server])
        nb_tries, response =0 , None
        error_message = ''
        header= {
            'ApiKey': credential.api_key,
        }
        try:
            while nb_tries<5 and response is None:
                try: 
                    req = requests.post(
                        api_url, json= shipment_request, timeout= TIMEOUT,
                        headers= header)
                    

                except ssl.SSLError as e:
                    error_message = e.reason
                    nb_tries += 1
                    continue
                req.raise_for_status()
                response = req.json()
        except requests.HTTPError as e:
            error_message = e.args[0]
        
        if error_message:
            raise SMSAError(
                gettext('stock_package_shipping_smsa.msg_smsa_webservice_error',
                        message= "%s \n %s" % (error_message, req.json()) )
            )
        
        if 'Status' in response:
            error_no = response['status']
            trace_id = response['traceId']
            error_title = response['title']

            message = '%s %s \n Trace ID: %s' % (response['status'], 
                                                 response['title'],
                                                 response['traceId'])
            raise SMSAError(
                gettext('stock_package_shipping_smsa.msg_smsa_webservice_error',
                        message= message)
            )
        if 'sawb' in response:
            shipment.reference = response['sawb']
            smsa_packages =  response['waybills']

            for tryton_pkg, smsa_pkg in zip_longest(packages, smsa_packages):
                label = fields.Binary.cast(base64.b64decode(
                    smsa_pkg['awbFile']
                ))
                tryton_pkg.shipping_reference = smsa_pkg['sawb']
                tryton_pkg.shipping_label = label
                tryton_pkg.shipping_label_mimetype = (
                    carrier.shipping_label_mimetype)
                
            Package.save(packages)
            shipment.save()

        return 'end'
    
    def get_credential_pattern(self, shipment):
        return {
            'company':  shipment.company.id
        }
    
    def get_credential(self, shipment):
        pool = Pool()
        SMSACredential =  pool.get('carrier.credential.smsa')
        credential_pattern = self.get_credential_pattern(shipment)
        
        for credential in SMSACredential.search([]):
            if credential.match(credential_pattern):
                return credential


    def get_shipment_weight(self, packages):
        pool = Pool()
        UoM = pool.get('product.uom')
        ModelData = pool.get('ir.model.data')
        kg = UoM(ModelData.get_id('product', 'uom_kilogram'))

        shipment_weight=0.0        
        for p in packages:
             if p.total_weight:
                shipment_weight+= UoM.compute_qty(p.weight_uom, p.total_weight, kg)
        return shipment_weight

    def get_shipment_value(self, shipment, packages):
        pool = Pool()
        Currency = pool.get('currency.currency')
        
        shipment_value= Decimal('0.0')
        for move in shipment.outgoing_moves:
            shipment_value += Decimal(Currency.compute(move.currency, move.unit_price,
                                    shipment.company.currency, round= False) * Decimal(move.quantity))
            
        return str(shipment_value)

    def get_request(self,shipment, packages, credential):

        recipient_address = shipment.shipping_to_address
        recipient_party = shipment.shipping_to

        sender_address = shipment.shipping_warehouse.address
        sender_party = shipment.company.party

        return {
            #TODO add optional fields
            'CurrencyCode': shipment.company.currency.code or '',
            'ItemsValue': self.get_shipment_value(shipment, packages),
            'NoOfPackages': len(packages),
            'OrderReference': shipment.number,
            'serviceTypeCode': shipment.carrier.smsa_service_type,
            'Weight(KG)': self.get_shipment_weight(packages),
            'ContentDescription': (shipment.shipping_description or '')[:50],
            'HoldAtSMSAOffice': False,
            #'retailCode': 1,
            'fridayShip': False,
            #'width': 12.3,
            #'height': 1.2,
            #'length': 2,
            #'latitude': '24.712094',
            #'longitude': '46.673689',
            'RecipientAddress': (recipient_address.street or '')[:35],
            #'RecipientDistrict': 'Al Rabie',
            'RecipientCityName': (recipient_address.city)[:30],
            'RecipientCountryCode': recipient_address.country.code if recipient_address.country else '',
            'RecipientName': recipient_party.full_name[:35],
            'RecipientPhone': recipient_address.contact_mechanism_get(
                {'phone', 'mobile'}).value,
            #'RecipientAlternativePhone': '966555555551',
            # 'RecipientID': '1234567890',
            'RecipientCompany': recipient_party.full_name[:35],
            'RecipientPostalCode': (recipient_address.postal_code 
                                    or '').replace(' ', '')[:9],
            'SenderAddress': (sender_address.street or '')[:35],
            'SenderCityName':  (sender_address.city or '')[:30],
            'SenderCountryCode': sender_address.country.code if sender_address.country else '',
            'SenderName': sender_party.full_name[:35],
            'SenderPhone': sender_address.contact_mechanism_get(
                {'phone', 'mobile'}).value,
            #'SenderAlternativePhone': '966555555550',
            'SenderPostalCode': (sender_address.postal_code 
                                    or '').replace(' ', '')[:9],
            'SenderCompany': sender_party.full_name[:35]
        }